# TP_Django

Cours SI2 - classe I3

Etudiants :
 - Cottet Emerald
 - Pantillon Kevin

Comment lancer le projet:
-------------------------

 - Créer un projet Django sur PyCharm
 - Importer les sources depuis le git
 - Depuis le répertoire du projet, effectuer les commandes suivantes:
   - python manage.py makemigrations
   - python manage.py migrate
 - Run le projet avec PyCharm (Maj+F10)
 - Se rendre à l'adresse http://127.0.0.1:8000/ ou autre adresse configurée