from django import forms

from GestionBudget.models import Item, Categories


class CategoryForm(forms.Form):
    categorie_nom = forms.CharField(max_length=50,
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control',
                                               'placeholder': 'Entrer un nom de categorie e.g. Nourriture',
                                               'aria-label': 'Categories', 'aria-describedby': 'add-btn'}))


class CategoryUpdateForm(forms.Form):
    categorie_nom = forms.CharField(max_length=50,
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control',
                                               'placeholder': 'Entrer un nom de categorie e.g. Nourriture',
                                               'aria-label': 'Categories', 'aria-describedby': 'add-btn'}))


class ItemForm(forms.Form):
    texte = forms.CharField(max_length=40,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control', 'placeholder': 'Entrer un nom e.g. Burger au McDonald',
                                       'aria-label': 'Item', 'aria-describedby': 'add-btn'}))

    choices = Categories.objects.all().values_list('categorie_nom', flat=True).order_by('id')
    categorie = forms.ModelChoiceField(queryset=choices)

    prix = forms.FloatField()


class ItemUpdateForm(forms.Form):
    texte = forms.CharField(max_length=40,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control', 'placeholder': 'Entrer un nom e.g. Burger au McDonald',
                                       'aria-label': 'Item', 'aria-describedby': 'add-btn'}))

    choices = Categories.objects.all().values_list('categorie_nom', flat=True).order_by('id')
    categorie = forms.ModelChoiceField(queryset=choices)

    prix = forms.FloatField()
