from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST

from .models import Item, Categories
from .forms import ItemForm, CategoryForm, CategoryUpdateForm, ItemUpdateForm


def home(request):
    item_list = Item.objects.order_by('id')

    category_list = Categories.objects.order_by('id')

    form = ItemForm()

    formCategory = CategoryForm()

    formUpdateCategory = CategoryUpdateForm()

    formUpdateItem = ItemUpdateForm()

    context = {'item_list': item_list, 'form': form, 'formCategory': formCategory, 'category_list': category_list,
               'formUpdateCategory': formUpdateCategory, 'formUpdateItem': formUpdateItem}

    return render(request, 'GestionBudget/home.html', context)


@require_POST
def addCategorie(request):
    form = CategoryForm(request.POST)

    if form.is_valid():
        new_cat = Categories(categorie_nom=request.POST['categorie_nom'])
        new_cat.save()

    return redirect('home')


def deleteCategorie(request):
    todel = request.POST.getlist('cat_to_del')
    print(todel)
    Categories.objects.filter(id__in=todel).delete()

    return redirect('home')


def updateCategorie(request, cat_id):
    form = CategoryUpdateForm(request.POST)

    if form.is_valid():
        Categories.objects.filter(pk=cat_id).update(categorie_nom=request.POST['categorie_nom'])

    return redirect('home')


@require_POST
def addItem(request):
    form = ItemForm(request.POST)

    new_cat = Categories.objects.get(categorie_nom=request.POST['categorie'])
    new_item = Item(texte=request.POST['texte'], categorie=new_cat,
                    prix=request.POST['prix'])
    new_item.save()

    return redirect('home')


def deleteItem(request):
    todel = request.POST.getlist('todelete')
    Item.objects.filter(id__in=todel).delete()

    return redirect('home')


def deleteAll(request):
    Item.objects.all().delete()

    return redirect('home')


def updateItem(request, item_id):
    form = ItemUpdateForm(request.POST)
    new_cat = Categories.objects.get(categorie_nom=request.POST['categorie'])

    Item.objects.filter(pk=item_id).update(texte=request.POST['texte'])
    Item.objects.filter(pk=item_id).update(prix=request.POST['prix'])
    Item.objects.filter(pk=item_id).update(categorie=new_cat)

    return redirect('home')
