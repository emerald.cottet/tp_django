from django.contrib import admin

# Register your models here.
from .models import Item, Categories  # this line added

admin.site.register(Item)
admin.site.register(Categories)
