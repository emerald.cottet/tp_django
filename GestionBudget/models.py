from django.db import models

# the following lines added:
import datetime
from django.utils import timezone


class Categories(models.Model):
    categorie_nom = models.CharField(max_length=50)

    def __str__(self):
        return self.categorie_nom


class Item(models.Model):
    texte = models.CharField(max_length=40)
    categorie = models.ForeignKey('Categories', on_delete=models.CASCADE)
    prix = models.FloatField()
    aSupprimer = models.BooleanField(default=False)

    def __str__(self):
        return self.texte
